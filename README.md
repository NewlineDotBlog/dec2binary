# dec2binary

A simple shell script that transforms an array of decimal numbers (1-255) to a binary file. This script was initially made to read out files from MSSQL databases.

## Usage

The script requires an input file named `file_in` in the same folder. This script expects the file to contain a line from a MS SQL database in CSV format (comma separated). For example:

```
"Filename","{'data': '[1,4,254,254...]'}","otherfields..."
```

This script will cut out the array based on its position between single quotes and returns this as `out.bin`.

If the `file_in` file is ready in the same folder, the script can be called as follows:

```
./dec2binary.sh
```
